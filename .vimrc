set mouse=""
set nocompatible
set number relativenumber
set nowrap
set encoding=utf-8
syntax on
set background=dark
set path=**
set expandtab
set hidden
set splitright
set showcmd
set tabstop=2
set shiftwidth=2
set softtabstop=2
set backspace=indent,eol,start
set autoindent
set incsearch
set nobackup
set nowritebackup
set noswapfile
set backupcopy=yes

let g:netrw_banner = 0 "no header spam in directory mode
let g:netrw_liststyle = 3
"let g:python3_host_prog='/usr/local/bin/python3.8'
"let g:syntastic_python_python_exec='/usr/local/bin/python3.8'
"let g:syntastic_python_flake8_exec='/usr/local/bin/python3.8'
let mapleader=","


" Enable folding
set foldmethod=indent
set foldlevel=99

"call plug#begin('~/.vim/plugged')
"Plug 'psf/black', { 'commit': 'ce14fa8b497bae2b50ec48b3bd7022573a59cdb1' }
"call plug#end()


set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'sheerun/vim-polyglot'
Plugin 'jeffkreeftmeijer/vim-numbertoggle'
Plugin 'posva/vim-vue'
Plugin 'ambv/black'
Plugin 'gmarik/Vundle.vim'
"Plugin 'dpelle/vim-Grammalecte'
Plugin 'arcticicestudio/nord-vim'
"Plugin 'altercation/vim-colors-solarized'
Plugin 'SirVer/ultisnips'
Plugin 'honza/vim-snippets'
Plugin 'dense-analysis/ale'
Plugin 'nvie/vim-flake8'
Plugin 'Yggdroot/indentLine'
Plugin 'itchyny/lightline.vim'
Plugin 'airblade/vim-gitgutter'
Plugin 'craigemery/vim-autotag'
Plugin 'mattn/emmet-vim'
Plugin 'tpope/vim-fugitive'
Plugin 'vim-syntastic/syntastic'
Plugin 'editorconfig/editorconfig-vim'
Plugin 'tweekmonster/django-plus.vim'
Plugin 'sillybun/vim-repl'
Plugin 'hashivim/vim-terraform'
Plugin 'tarekbecker/vim-yaml-formatter'
call vundle#end()

colorscheme nord
set updatetime=750

let g:lightline = {
      \'colorscheme': 'nord',
      \}

highlight BadWhitespace ctermbg=red guibg=red
filetype on
filetype plugin indent on
filetype indent on

" vuejs
au BufNewFile,BufRead *.vue setf vue
autocmd BufRead,BufNewFile *.vue setlocal filetype=vue.html.javascript.css
let g:vim_vue_plugin_load_full_syntax = 1
let g:vim_vue_plugin_debug = 1

" snippet
"
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsSnippetDirectories=["UltiSnips", "my_snippets"]

"REPL
let g:repl_program = {
      \   'python': '/usr/local/bin/docker-compose exec api ./manage.py shell_plus',
      \   'default' : 'bash'
      \   }
autocmd Filetype python nnoremap <F12> <Esc>:REPLDebugStopAtCurrentLine<Cr>
autocmd Filetype python nnoremap <F10> <Esc>:REPLPDBN<Cr>
autocmd Filetype python nnoremap <F11> <Esc>:REPLPDBS<Cr>
nnoremap <leader>r :REPLToggle<Cr>
let g:repl_position = 3
let g:repl_python_automerge = 1

"syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_python_python_exec = 'python3'

"Powerline
set laststatus=2
set showtabline=2
set t_Co=256

"Eslint
let g:ale_fix_on_save = 1
let g:ale_completion_enabled = 1

let g:ale_linter_aliases = {'vue': ['vue', 'javascript']}
let g:ale_linters = {'vue': ['eslint', 'vls']}

let g:ale_fixers = {
      \   '*': ['remove_trailing_lines', 'trim_whitespace'],
      \   'javascript': ['eslint', 'prettier'],
      \   'vue': ['eslint', 'prettier'],
      \   'htmldjango': ['prettier'],
      \}

" terraform
let g:terraform_align=1
let g:terraform_fold_sections=1
let g:terraform_fmt_on_save=1

"Emmet
let g:user_emmet_install_global = 0
autocmd FileType htmldjango,html,scss,css EmmetInstall
let g:user_emmet_expandabbr_key = '<c-Right>'
"augroup EmmetSettings
"  autocmd! FileType htmldjango imap <tab-a> <plug>(emmet-expand-abbr)
"  autocmd! FileType html imap <tab> <plug>(emmet-expand-abbr)
"  autocmd! FileType css imap <tab> <plug>(emmet-expand-abbr)
"  autocmd! FileType scss imap <tab> <plug>(emmet-expand-abbr)
"augroup END

"80 columns stuff
let &colorcolumn=join(range(81,999),",")

"Once you go black ...
let g:black_linelength=79
autocmd BufWritePre *.py execute ':Black'

"let g:grammalecte_cli_py='~/bin/grammalecte/grammalecte-cli.py'

autocmd FileType python map <buffer> <F9> :w<CR>:exec '!python3' shellescape(@%, 1)<CR>
autocmd FileType python imap <buffer> <F9> <esc>:w<CR>:exec '!python3' shellescape(@%, 1)<CR>

"yaml
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
set is hlsearch ai ic scs
nnoremap <esc><esc> :nohls<cr>
